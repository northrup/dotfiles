#!/bin/bash
# choose pinentry depending on PINENTRY_USER_DATA
# requires pinentry-curses and pinentry-gtk2
# this *only works* with gpg 2
# see https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=802020

case $PINENTRY_USER_DATA in
gui)
  exec /usr/local/bin/pinentry-mac "$@"
  ;;
none)
  exit 1 # do not ask for passphrase
  ;;
*)
  exec /usr/local/bin/pinentry-curses "$@"
esac
