#!/bin/bash
case $# in
   0)
echo "Usage: $0 {start|stop}"
exit 1
      ;;
   1)
case $1 in
         start)
echo "Starting OpenDNS Client..."
            launchctl load /Library/LaunchAgents/com.cisco.umbrella.menu.plist
            sudo launchctl load /Library/LaunchDaemons/com.opendns.osx.RoamingClientConfigUpdater.plist
echo "Done!"
            ;;
         stop)
echo "Stopping OpenDNS Client..."
            sudo launchctl remove com.opendns.osx.RoamingClientConfigUpdater
            launchctl remove com.cisco.umbrella.menu
echo "Done!"
            ;;
*)
echo "'$1' is not a valid verb."
echo "Usage: $0 {start|stop}"
exit 2
            ;;
esac
      ;;
*)
echo "Too many args provided ($#)."
echo "Usage: $0 {start|stop}"
exit 3
      ;;
esac
