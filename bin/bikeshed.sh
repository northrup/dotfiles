#!/bin/bash
# bin/bikeshed

WPS_00="10.255.5.9" # webpowerswitch

VCONSOLE="10.255.12.200" # vcenter

IPMI_05="10.255.5.105" # esx05-ipmi
IPMI_06="10.255.5.106" # esx06-ipmi
IPMI_07="10.255.5.107" # esx07-ipmi

ESX_05="10.255.12.205" # esx05
ESX_06="10.255.12.206" # esx06
ESX_07="10.255.11.207" # esx07

NAS05="10.255.11.212" # synology rs1219+ crate00

SOEKRIS="75.41.75.93" # hoenir, byggvir, whichever is active

sudo ifconfig lo0 alias 127.0.0.3   # nas05
sudo ifconfig lo0 alias 127.0.0.5   # esx05
sudo ifconfig lo0 alias 127.0.0.6   # esx06
sudo ifconfig lo0 alias 127.0.0.7   # esx07
sudo ifconfig lo0 alias 127.0.0.100 # vcsa00

# vcenter
sudo ssh -A \
  -L 127.0.0.1:8100:${WPS_00}:80 \
  \
  -L 127.0.0.1:8105:${IPMI_05}:443 \
  -L 127.0.0.1:8106:${IPMI_06}:443 \
  -L 127.0.0.1:8107:${IPMI_07}:443 \
  \
  -L 127.0.0.3:9443:${NAS05}:443 \
  -L 127.0.0.3:5001:${NAS05}:5001 \
  \
  -L 127.0.0.100:443:${VCONSOLE}:443 \
  -L 127.0.0.100:902:${VCONSOLE}:902 \
  -L 127.0.0.100:903:${VCONSOLE}:903 \
  -L 127.0.0.100:5480:${VCONSOLE}:5480 \
  \
  -L 127.0.0.5:443:${ESX_05}:443 \
  -L 127.0.0.5:902:${ESX_05}:902 \
  -L 127.0.0.5:903:${ESX_05}:903 \
  -L 127.0.0.5:5480:${ESX_05}:5480 \
  \
  -L 127.0.0.6:443:${ESX_06}:443 \
  -L 127.0.0.6:902:${ESX_06}:902 \
  -L 127.0.0.6:903:${ESX_06}:903 \
  -L 127.0.0.6:5480:${ESX_06}:5480 \
  \
  -L 127.0.0.7:443:${ESX_07}:443 \
  -L 127.0.0.7:902:${ESX_07}:902 \
  -L 127.0.0.7:903:${ESX_07}:903 \
  -L 127.0.0.7:5480:${ESX_07}:5480 \
  \
  -o UserKnownHostsFile=/dev/null \
  -o StrictHostKeyChecking=no \
  -p 2222 root@${SOEKRIS}
