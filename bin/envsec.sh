#!/bin/bash
#
case $# in
0)
	echo "Usage: $0 {set|clear}"
	exit 1
	;;
1)
	case $1 in
	set)
		echo "Setting ENV variable secrets..."
		echo "...setting BUNDLE_RUBYGEMS__PKG__GITHUB__COM"
		export BUNDLE_RUBYGEMS__PKG__GITHUB__COM="northrup:$(op read --account 8bitwizard.1password.com op://Personal/mfwsi5ujbxayglq64m65jizvpi/credential)"
		echo "...setting GITHUB_CONTAINER_REGISTRY_RO"
		export GITHUB_CONTAINER_REGISTRY_RO="$(op read --account 8bitwizard.1password.com op://Personal/mfwsi5ujbxayglq64m65jizvpi/credential)"
		echo "Done!"
		;;
	clear)
		echo "Clearing ENV variable secrets..."
		unset BUNDLE_RUBYGEMS__PKG__GITHUB__COM
		echo "Done!"
		;;
	*)
		echo "'$1' is not a valid verb for this command."
		echo "Usage: $0 {set|clear}"
		exit 2
		;;
	esac
	;;
*)
	echo "Too many args provided ($#)."
	echo "Usage: $0 {set|clear}"
	exit 3
	;;
esac
