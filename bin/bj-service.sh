#!/bin/bash
case $# in
   0)
echo "Usage: $0 {stop}"
exit 1
      ;;
   1)
case $1 in
         stop)
echo "Stopping BlueJeans..."
            launchctl remove com.bluejeansnet.BlueJeansHelper
            launchctl remove com.bluejeansnet.BlueJeansMenu
echo "Done!"
            ;;
*)
echo "'$1' is not a valid verb."
echo "Usage: $0 {stop}"
exit 2
            ;;
esac
      ;;
*)
echo "Too many args provided ($#)."
echo "Usage: $0 {stop}"
exit 3
      ;;
esac
