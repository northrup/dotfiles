#!/bin/sh

REPOS=`sed '/^#/d' /Users/northrup/GitHub/github.com/github/sae-iam-team/docs/reference/owned_repos.txt | sed 's/^/github\//' | grep -v "github/entitlements" | paste -sd "," -`
gh search prs -R $REPOS $* 
