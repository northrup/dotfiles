#!/usr/bin/env bash

for pr in $(cat $1)
do
   echo "Pulling diff for $pr"
   gh pr diff "$pr"

   echo "Approve this pull request?"

while true; do
    read -p "Do you wish to approve this PR? " yn
    case $yn in
        [Yy]* ) gh pr review --approve "$pr";
            break;;
        [Nn]* ) break;;
        * ) echo "Please answer yes or no.";;
    esac
done
done
