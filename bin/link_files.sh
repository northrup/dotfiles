#!/bin/sh

dot_env="$HOME/.valhalla"
zshfiles="$dot_env/zsh/hel/core"
tmuxfiles="$dot_env/tmux"
gitfiles="$dot_env/git"
localdir="$dot_env/local"
configdir="$dot_env/config"

#
# Zsh Dotfiles
#

echo ""
if [ -d "$zshfiles" ]; then
  echo "Symlinking zsh dotfiles from $zshfiles"
  for location in $(find $zshfiles -name '*' -type f); do
    file="${location##*/}"
    file="${file%.sh}"
    link "$location" "$HOME/.$file"
  done
else
  echo "$zshfiles does not exist"
  exit 1
fi



#
# Tmux Dotfiles
#

echo ""
if [ -d "$tmuxfiles" ]; then
  echo "Symlinking tmux dotfiles from $tmuxfiles"
  for location in $(find $tmuxfiles -maxdepth 1 \! -name "plugins" -name '*' ); do
    file="${location##*/}"
    file="${file%.sh}"
    link "$location" "$HOME/.$file"
  done
else
  echo "$tmuxfiles does not exist"
  exit 1
fi

#
# git Dotfiles
#

echo ""
if [ -d "$gitfiles" ]; then
  echo "Symlinking zsh dotfiles from $gitfiles"
  for location in $(find $gitfiles -name '*' -type f \! -name "gitconfig-*" ); do
    file="${location##*/}"
    file="${file%.sh}"
    link "$location" "$HOME/.$file"
  done
else
  echo "$gitfiles does not exist"
  exit 1
fi



#
# Local Directory
#
echo ""
if [ -d "$localdir" ]; then
  echo "Symlinking local directory from $localdir"
    localdirtgt="${localdir##*/}"
    link "$localdir" "$HOME/.$localdirtgt"
else
  echo "$localdir does not exist"
  exit 1
fi

#
# Config Directory
#
echo ""
if [ -d "$configdir" ]; then
  echo "Symlinking config directory from $configdir"
    configdirtgt="${configdir##*/}"
    link "$configdir" "$HOME/.$configdirtgt"
else
  echo "$configdir does not exist"
  exit 1
fi

##
## Link Function
##
link() {
  from="$1"
  to="$2"
  echo "Linking '$from' to '$to'"
  /bin/rm -rf "$to"
  /bin/ln -sfF "$from" "$to"
}

