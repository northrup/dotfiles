#
# Defines macOS aliases and functions.
#
# Authors:
#   Sorin Ionescu <sorin.ionescu@gmail.com>
#

# Return if requirements are not found.
if [[ "$OSTYPE" != darwin* ]]; then
  return 1
fi

#
# Aliases
#
alias flushdns="sudo dscacheutil -flushcache; sudo killall -HUP mDNSResponder; say DNS cache is now flushed"

# Changes directory to the current Finder directory.
alias cdf='cd "$(pfd)"'

# Let's fuck up less
if [[ -s "/usr/local/bin/trash" ]]; then
  alias rm='trash'
fi
if [[ -s "/opt/homebrew/bin/trash" ]]; then
  alias rm='trash'
fi

# Pushes directory to the current Finder directory.
alias pushdf='pushd "$(pfd)"'
