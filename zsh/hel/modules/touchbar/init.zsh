#
# Provides Git aliases and functions.
#

# Return if requirements are not found.
if (( ! $+commands[git] )); then
  return 1
fi

# Load dependencies.
hmodload 'helper'

# Source module files.
source "${0:h}/touchbar.zsh"
