#
# Initializes Hel
#
# Authors:
#   John Northrup <john@8bitwizard.net>
#

#
# Version Check
#

# Check for the minimum supported version.
min_zsh_version='5.7.1'
if ! autoload -Uz is-at-least || ! is-at-least "$min_zsh_version"; then
  printf "Hel: Thou shalt upgrade your self, minimum version required is: %s\n" "$min_zsh_version" >&2
  return 1
fi
unset min_zsh_version

#
# Module Loader
#

# Loads hel modules.
function hmodload {
  local -a hmodules
  local -a hmodule_dirs
  local -a locations
  local hmodule
  local hmodule_location
  local hfunction_glob='^([_.]*|prompt_*_setup|README*|*~)(-.N:t)'

  # Load in any additional directories and warn if they don't exist
  zstyle -a ':hel:load' hmodule-dirs 'user_hmodule_dirs'
  for user_dir in "$user_hmodule_dirs[@]"; do
    if [[ ! -d "$user_dir" ]]; then
      echo "$0: Missing user module dir: $user_dir"
    fi
  done

  hmodule_dirs=("$ZHELDIR/modules" "$ZHELDIR/contrib" "$user_hmodule_dirs[@]")

  # $argv is overridden in the anonymous function.
  hmodules=("$argv[@]")

  # Load Hel modules.
  for hmodule in "$hmodules[@]"; do
    if zstyle -t ":hel:module:$hmodule" loaded 'yes' 'no'; then
      continue
    else
      locations=(${hmodule_dirs:+${^hmodule_dirs}/$hmodule(-/FN)})
      if (( ${#locations} > 1 )); then
        print "$0: conflicting module locations: $locations"
        continue
      elif (( ${#locations} < 1 )); then
        print "$0: no such module: $hmodule"
        continue
      fi

      # Grab the full path to this module
      hmodule_location=${locations[1]}

      # Add functions to $fpath.
      fpath=(${hmodule_location}/functions(/FN) $fpath)

      function {
        local hfunction

        # Extended globbing is needed for listing autoloadable function directories.
        setopt LOCAL_OPTIONS EXTENDED_GLOB

        # Load Prezto functions.
        for hfunction in ${hmodule_location}/functions/$~hfunction_glob; do
          autoload -Uz "$hfunction"
        done
      }

      if [[ -s "${hmodule_location}/init.zsh" ]]; then
        source "${hmodule_location}/init.zsh"
      elif [[ -s "${hmodule_location}/${hmodule}.plugin.zsh" ]]; then
        source "${hmodule_location}/${hmodule}.plugin.zsh"
      fi

      if (( $? == 0 )); then
        zstyle ":hel:module:$hmodule" loaded 'yes'
      else
        # Remove the $fpath entry.
        fpath[(r)${hmodule_location}/functions]=()

        function {
          local hfunction

          # Extended globbing is needed for listing autoloadable function
          # directories.
          setopt LOCAL_OPTIONS EXTENDED_GLOB

          # Unload Prezto functions.
          for hfunction in ${hmodule_location}/functions/$~hfunction_glob; do
            unfunction "$hfunction"
          done
        }

        zstyle ":hel:module:$hmodule" loaded 'no'
      fi
    fi
  done
}

#
# Hel Initialization
#

# This finds the directory prezto is installed to so plugin managers don't need
# to rely on dirty hacks to force prezto into a directory. Additionally, it
# needs to be done here because inside the hmodload function ${0:h} evaluates to
# the current directory of the shell rather than the prezto dir.
ZHELDIR=${0:h}

# Source the Prezto configuration file.
if [[ -s "${ZDOTDIR:-$HOME}/.zhelrc" ]]; then
  source "${ZDOTDIR:-$HOME}/.zhelrc"
fi

# Disable color and theme in dumb terminals.
if [[ "$TERM" == 'dumb' ]]; then
  zstyle ':hel:*:*' color 'no'
  zstyle ':hel:module:prompt' theme 'off'
fi

# Load Zsh modules.
zstyle -a ':hel:load' zmodule 'zmodules'
for zmodule ("$zmodules[@]") zmodload "zsh/${(z)zmodule}"
unset zmodule{s,}

# Autoload Zsh functions.
zstyle -a ':hel:load' zfunction 'zfunctions'
for zfunction ("$zfunctions[@]") autoload -Uz "$zfunction"
unset zfunction{s,}

# Load Hel modules.
zstyle -a ':hel:load' hmodule 'hmodules'
hmodload "$hmodules[@]"
unset hmodules
