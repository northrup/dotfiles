-- Pull in the wezterm API
local wezterm = require("wezterm")

local bg_aliens = require("bg_aliens")
local tmux_bindings = require("tmux_bindings")

local tabline = wezterm.plugin.require("https://github.com/michaelbrusegard/tabline.wez")

-- This will hold the configuration.
local config = wezterm.config_builder()

config.color_scheme = "Tokyo Night"

config.initial_rows = 34
config.initial_cols = 138

config.enable_scroll_bar = false
config.min_scroll_bar_height = "2cell"
config.colors = {
	scrollbar_thumb = "white",
}

config.unix_domains = { { name = "unix" } }

-- load in the alien background
bg_aliens.apply_to_config(config)

-- load tmux bindings
tmux_bindings.apply_to_config(config)

-- load tabline in wezterm
local function tab_title(tab_info)
	local title = tab_info.tab_title
	-- if the tab title is explicitly set, take that
	if title and #title > 0 then
		return title
	end
	-- Otherwise, use the title from the active pane
	-- in that tab
	return tab_info.active_pane.title
end

tabline.setup({
	options = {
		icons_enabled = true,
		theme = "Tokyo Night",
		tabs_enabled = true,
		theme_overrides = {},
		section_separators = {
			left = wezterm.nerdfonts.pl_left_hard_divider,
			right = wezterm.nerdfonts.pl_right_hard_divider,
		},
		component_separators = {
			left = wezterm.nerdfonts.pl_left_soft_divider,
			right = wezterm.nerdfonts.pl_right_soft_divider,
		},
		tab_separators = {
			left = wezterm.nerdfonts.pl_left_hard_divider,
			right = wezterm.nerdfonts.pl_right_hard_divider,
		},
	},
	sections = {
		tabline_a = { "mode" },
		tabline_b = { "workspace" },
		tabline_c = { " " },
		tab_active = {
			"index",
			tab_title,
			{ "zoomed", padding = 0 },
		},
		tab_inactive = { "index", { "process", padding = { left = 0, right = 1 } } },
		tabline_x = { " " },
		tabline_y = { "datetime", "battery" },
		tabline_z = { "domain" },
	},
	extensions = {},
})
tabline.apply_to_config(config)

-- Visual Bell
config.visual_bell = {
	fade_in_function = "EaseIn",
	fade_in_duration_ms = 150,
	fade_out_function = "EaseOut",
	fade_out_duration_ms = 150,
	target = "CursorColor",
}

-- and finally, return the configuration to wezterm
return config
