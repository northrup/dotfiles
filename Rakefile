# frozen_string_literal: true

require 'rake'
require 'fileutils'

desc 'Wire up the Northrup dotfiles'
task :install => [:submodule_init, :submodules] do
  puts
  puts '======================================================'
  puts " Let's install our dot files."
  puts '======================================================'
  puts

  install_homebrew if RUBY_PLATFORM.downcase.include?('darwin')

  file_operation(FileList['zsh/hel/core/*']) if want_to_install?('zsh configs')
  file_operation(FileList['p10k/*']) if want_to_install?('zsh powerlevel10k configs')
  file_operation(FileList['git/*'].exclude(/gitconfig-/)) if want_to_install?('git configs')
  file_operation(FileList['irb/*']) if want_to_install?('irb and pry configs')
  file_operation(FileList['tmux/tmux.*']) if want_to_install?('tmux configs')
  file_operation(FileList['github/ldap/*']) if want_to_install?('GitHub LDAP Configuration')
  file_operation(['config']) if want_to_install?('base config directory')
  file_operation(['local']) if want_to_install?('base local directory')
  file_operation(['tmux']) if want_to_install?('base tmux plugins')
  file_operation(['calendar']) if want_to_install?('pithy calendar entries')
  file_operation(['hammerspoon']) if want_to_install?('hammerspoon config and spoons')

  run_bundle_config

  success_msg('installed')
end

task :success do
  success_msg('installed')
end
task :submodule_init do
  unless ENV['SKIP_SUBMODULES']
    run %( git submodule update --init --recursive )
  end
end

desc 'Init and update submodules.'
task :submodules do
  unless ENV['SKIP_SUBMODULES']
    puts '======================================================'
    puts 'Downloading submodules...please wait'
    puts '======================================================'

    run %{
      cd $HOME/.valhalla
      git submodule update --recursive
    }
    puts
  end
end

private

def run(cmd)
  puts "[Running] #{cmd}"
  `#{cmd}` unless ENV['DEBUG']
end

def number_of_cores
  if RUBY_PLATFORM.downcase.include?('darwin')
    cores = run %{ sysctl -n hw.ncpu }
  else
    cores = run %{ nproc }
  end
  puts
  cores.to_i
end

def run_bundle_config
  return unless system('which bundle')

  bundler_jobs = number_of_cores - 1
  puts '======================================================'
  puts 'Configuring Bundlers for parallel gem installation'
  puts '======================================================'
  run %{ bundle config --global jobs #{bundler_jobs} }
  puts
end

def install_rvm_binstubs
  puts '======================================================'
  puts 'Installing RVM Bundler support. Never have to type'
  puts 'bundle exec again! Please use bundle --binstubs and RVM'
  puts "will automatically use those bins after cd'ing into dir."
  puts '======================================================'
  run %{ chmod +x $rvm_path/hooks/after_cd_bundler }
  puts
end

def install_homebrew
  run %{which brew}
  unless $?.success?
    puts "======================================================"
    puts "Installing Homebrew, the OSX package manager...If it's"
    puts "already installed, this will do nothing."
    puts "======================================================"
    run %{ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"}
  end

  puts
  puts
  puts "======================================================"
  puts "Updating Homebrew."
  puts "======================================================"
  run %{brew update}
  puts
  puts
  puts "======================================================"
  puts "Installing Homebrew packages...There may be some warnings."
  puts "======================================================"
  run %{brew tap homebrew/bundle}
  run %{brew tap homebrew/services}
  run %{brew tap homebrew/cask-fonts}
  run %{brew tap derailed/k9s}
  run %{brew install git gh hub zsh neovim tmux gnupg pinentry-mac}
  run %{brew install rbenv ruby-build}
  run %{brew install httpie coreutils git-quick-stats gnu-sed findutils jq libssh2 fzf trash vault}
  run %{brew install oath-toolkit socat ssh-copy-id the_silver_searcher htop}
  run %{brew install pwgen watch vault ctags git-delta dive git-lfs nmap rclone mas}
  run %{brew install cmatrix fortune tree-sitter ripgrep no-more-secrets}
  run %{brew install mtr iperf3}
  run %{brew install helm kubernetes-cli k9s kind cfssl}
  run %{brew install lua luajit luajit-openresty luarocks}
  run %{brew install --cask font-fira-code}
  run %{brew install --cask font-hack-nerd-font}
  run %{brew install --cask font-victor-mono}
  run %{brew install --cask wezterm}
  run %{brew install --cask nikitabobko/tap/aerospace}
  puts
  puts
end

def install_term_theme
  puts "======================================================"
  puts "Installing iTerm2 solarized theme."
  puts "======================================================"
  run %{ /usr/libexec/PlistBuddy -c "Add :'Custom Color Presets':'Solarized Light' dict" ~/Library/Preferences/com.googlecode.iterm2.plist }
  run %{ /usr/libexec/PlistBuddy -c "Merge 'iTerm2/Solarized Light.itermcolors' :'Custom Color Presets':'Solarized Light'" ~/Library/Preferences/com.googlecode.iterm2.plist }
  run %{ /usr/libexec/PlistBuddy -c "Add :'Custom Color Presets':'Solarized Dark' dict" ~/Library/Preferences/com.googlecode.iterm2.plist }
  run %{ /usr/libexec/PlistBuddy -c "Merge 'iTerm2/Solarized Dark.itermcolors' :'Custom Color Presets':'Solarized Dark'" ~/Library/Preferences/com.googlecode.iterm2.plist }

  # If iTerm2 is not installed or has never run, we can't autoinstall the profile since the plist is not there
  if !File.exist?(File.join(ENV['HOME'], '/Library/Preferences/com.googlecode.iterm2.plist'))
    puts "======================================================"
    puts "To make sure your profile is using the solarized theme"
    puts "Please check your settings under:"
    puts "Preferences> Profiles> [your profile]> Colors> Load Preset.."
    puts "======================================================"
    return
  end

  # Ask the user which theme he wants to install
  message = "Which theme would you like to apply to your iTerm2 profile?"
  color_scheme = ask message, iTerm_available_themes

  return if color_scheme == 'None'

  color_scheme_file = File.join('iTerm2', "#{color_scheme}.itermcolors")

  # Ask the user on which profile he wants to install the theme
  profiles = iTerm_profile_list
  message = "I've found #{profiles.size} #{profiles.size>1 ? 'profiles': 'profile'} on your iTerm2 configuration, which one would you like to apply the Solarized theme to?"
  profiles << 'All'
  selected = ask message, profiles

  if selected == 'All'
    (profiles.size-1).times { |idx| apply_theme_to_iterm_profile_idx idx, color_scheme_file }
  else
    apply_theme_to_iterm_profile_idx profiles.index(selected), color_scheme_file
  end
end

def ask(message, values)
  puts message
  while true
    values.each_with_index { |val, idx| puts " #{idx+1}. #{val}" }
    selection = STDIN.gets.chomp
    if (Float(selection)==nil rescue true) || selection.to_i < 0 || selection.to_i > values.size+1
      puts "ERROR: Invalid selection.\n\n"
    else
      break
    end
  end
  selection = selection.to_i - 1
  values[selection]
end

def want_to_install? (section)
  if ENV["ASK"]=="true"
    puts "Would you like to install configuration files for: #{section}? [y]es, [n]o"
    STDIN.gets.chomp == 'y'
  else
    true
  end
end

def file_operation(files, method = :symlink)
  files.each do |f|
    file = f.split('/').last
    source = "#{ENV["PWD"]}/#{f}"
    target = "#{ENV["HOME"]}/.#{file}"

    puts "======================#{file}=============================="
    puts "Source: #{source}"
    puts "Target: #{target}"

    if File.exist?(target) && (!File.symlink?(target) || (File.symlink?(target) && File.readlink(target) != source))
      puts "[Overwriting] #{target}...leaving original at #{target}.backup..."
      run %{ mv "$HOME/.#{file}" "$HOME/.#{file}.backup" }
    end

    if method == :symlink
      run %{ ln -nfs "#{source}" "#{target}" }
    else
      run %{ cp -f "#{source}" "#{target}" }
    end

    puts "=========================================================="
    puts
  end
end

def list_vim_submodules
  result=`git submodule -q foreach 'echo $name"||"\`git remote -v | awk "END{print \\\\\$2}"\`'`.select{ |line| line =~ /^vim.bundle/ }.map{ |line| line.split('||') }
  Hash[*result.flatten]
end

def apply_theme_to_iterm_profile_idx(index, color_scheme_path)
  values = Array.new
  16.times { |i| values << "Ansi #{i} Color" }
  values << ['Background Color', 'Bold Color', 'Cursor Color', 'Cursor Text Color', 'Foreground Color', 'Selected Text Color', 'Selection Color']
  values.flatten.each { |entry| run %{ /usr/libexec/PlistBuddy -c "Delete :'New Bookmarks':#{index}:'#{entry}'" ~/Library/Preferences/com.googlecode.iterm2.plist } }

  run %{ /usr/libexec/PlistBuddy -c "Merge '#{color_scheme_path}' :'New Bookmarks':#{index}" ~/Library/Preferences/com.googlecode.iterm2.plist }
  run %{ defaults read com.googlecode.iterm2 }
end

def success_msg(action)
  puts ""
  puts "                                                 *                                             "
  puts "                                         *     **                                              "
  puts "                                        **     **                                              "
  puts "                                        **     **                                              "
  puts "                ****    ***  ****     ******** **        ***  ****    **   ****        ****    "
  puts "***  ****      * ***  *  **** **** * ********  **  ***    **** **** *  **    ***  *   * ***  * "
  puts " **** **** *  *   ****    **   ****     **     ** * ***    **   ****   **     ****   *   ****  "
  puts "  **   ****  **    **     **            **     ***   ***   **          **      **   **    **   "
  puts "  **    **   **    **     **            **     **     **   **          **      **   **    **   "
  puts "  **    **   **    **     **            **     **     **   **          **      **   **    **   "
  puts "  **    **   **    **     **            **     **     **   **          **      **   **    **   "
  puts "  **    **   **    **     **            **     **     **   **          **      **   **    **   "
  puts "  **    **    ******      ***           **     **     **   ***          ******* **  *******    "
  puts "  ***   ***    ****        ***           **    **     **    ***          *****   ** ******     "
  puts "   ***   ***                                    **    **                            **         "
  puts "                                                      *                             **         "
  puts "                                                     *                              **         "
  puts "                                                    *                                **        "
  puts "                                                   *                                           "
  puts "                                                                                               "
  puts "     **                               ***           ***                                        "
  puts "      **                  *         ** ***    *      ***                                       "
  puts "      **                 **        **   ***  ***      **                                       "
  puts "      **                 **        **         *       **                                       "
  puts "      **      ****     ********    **                 **                 ****                  "
  puts "  *** **     * ***  * ********     ******   ***       **       ***      * **** *               "
  puts " *********  *   ****     **        *****     ***      **      * ***    **  ****                "
  puts "**   ****  **    **      **        **         **      **     *   ***  ****                     "
  puts "**    **   **    **      **        **         **      **    **    ***   ***                    "
  puts "**    **   **    **      **        **         **      **    ********      ***                  "
  puts "**    **   **    **      **        **         **      **    *******         ***                "
  puts "**    **   **    **      **        **         **      **    **         ****  **                "
  puts "**    **    ******       **        **         **      **    ****    * * **** *                 "
  puts " *****       ****         **       **         *** *   *** *  *******     ****                  "
  puts "  ***                               **         ***     ***    *****                            "
  puts ""
  puts "Your dot files have been #{action}. Restart your terminal get back to work."
  puts ""
end
